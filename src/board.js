const PIXI = require('pixi.js');

class Board {
  constructor(global) {
    this.global = global;

    this.graphics = null;
    this.drawingPlayer = '';
    this.update();
    
    this.gameListener = this.global.emitter.on('engineDone', this.update.bind(this));
  }
  update() {
    //Create board if needed
    if(this.graphics === null) {
      this.graphics = new PIXI.Graphics();
      this.global.boardLayer.addChild(this.graphics);
    }
    if(this.drawingPlayer !== this.global.state.player) {
      this.drawingPlayer = this.global.state.player;
      this.graphics.clear();

      this.graphics.beginFill(this.drawingPlayer === 'white' ? 0x000000 : 0xffffff);
      this.graphics.drawRoundedRect(0, 0, 1000, 1000);
      this.graphics.beginFill(this.drawingPlayer === 'white' ? 0xffffff : 0x000000);
      this.graphics.drawRoundedRect(20, 20, 960, 960);
      
      for(var f = 1;f <= 8;f++) {
        for(var r = 1;r <= 8;r++) {
          this.graphics.beginFill(f % 2 === r % 2 ? 0x86a666 : 0xffffdd);
          if(this.drawingPlayer === 'white') {
            this.graphics.beginFill(f % 2 !== r % 2 ? 0x86a666 : 0xffffdd);
          }
          this.graphics.drawRect(f*100, r*100, 100, 100);
        }
      }
      this.graphics.endFill();
    }
  }
  destroy() {
    this.gameListener();
  }
};

module.exports = Board;