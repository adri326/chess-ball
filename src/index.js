const { Engine, Bodies, Composite } = require('matter-js');
const PIXI = require('pixi.js');
const { Viewport } = require('pixi-viewport');
const { createNanoEvents } = require('nanoevents');

const Game = require('@local/game');

//Global variable to store everything
window.global = {};
global = window.global;

//Setup Nanoevents
global.emitter = createNanoEvents();

//Setup PIXI App
global.app = new PIXI.Application({ backgroundColor: 0xa38399 });
document.body.appendChild(global.app.view);
global.app.resizeTo = window;

//Setup PIXI Viewport
global.viewport = new Viewport({
  screenWidth: window.innerWidth,
  screenHeight: window.innerHeight,
  worldWidth: 1000,
  worldHeight: 1000,
});
var resizeViewport = () => {
  global.viewport.resize(window.innerWidth, window.innerHeight);
  if(window.innerHeight < window.innerWidth) {
    global.viewport.snapZoom({ height: 1200, time: 250 });
  }
  else {
    global.viewport.snapZoom({ width: 1200, time: 250 });
  }
  global.viewport.snap(500, 500, { time: 250 });
};
resizeViewport();
window.addEventListener('resize', resizeViewport.bind(this));
global.app.stage.addChild(global.viewport);

//Setup PIXI Layers
global.boardLayer = new PIXI.Container();
global.pieceLayer = new PIXI.Container();
global.overlayLayer = new PIXI.Container();
global.guiLayer = new PIXI.Container();
global.viewport.addChild(global.boardLayer);
global.viewport.addChild(global.pieceLayer);
global.viewport.addChild(global.overlayLayer);
global.app.stage.addChild(global.guiLayer);

//Setup Matterjs world
global.engine = Engine.create();
global.engine.gravity.y = 0;
global.engine.enableSleeping = true;

global.bounds = {};
global.bounds.top = Bodies.rectangle(400,-50,1000,100, { isStatic: true });
global.bounds.top.restitution = 0.5;
global.bounds.bottom = Bodies.rectangle(400,850,1000,100, { isStatic: true });
global.bounds.bottom.restitution = 0.5;
global.bounds.left = Bodies.rectangle(-50,400,100,1000, { isStatic: true });
global.bounds.left.restitution = 0.5;
global.bounds.right = Bodies.rectangle(850,400,100,1000, { isStatic: true });
global.bounds.right.restitution = 0.5;
Composite.add(global.engine.world, [global.bounds.top, global.bounds.bottom, global.bounds.left, global.bounds.right]);

window.game = new Game(global);