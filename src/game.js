const { Engine, Events } = require('matter-js');
const Board = require('@local/board');
const Piece = require('@local/piece');
const Network = require('@local/network');

class Game {
  constructor(global) {
    this.global = global;
    //Clearing state
    this.global.state = {
      type: 'local',
      player: 'white',
      canPlay: true,
      isActive: false,
      pieces: [
        { id: 0, x: 50, y: 50, type: 'rook', player: 'black' },
        { id: 2, x: 250, y: 50, type: 'bishop', player: 'black' },
        { id: 3, x: 350, y: 50, type: 'queen', player: 'black' },
        { id: 4, x: 450, y: 50, type: 'king', player: 'black' },
        { id: 5, x: 550, y: 50, type: 'bishop', player: 'black' },
        { id: 7, x: 750, y: 50, type: 'rook', player: 'black' },
        { id: 8, x: 50, y: 150, type: 'pawn', player: 'black' },
        { id: 9, x: 150, y: 150, type: 'pawn', player: 'black' },
        { id: 10, x: 250, y: 150, type: 'pawn', player: 'black' },
        { id: 11, x: 350, y: 150, type: 'pawn', player: 'black' },
        { id: 12, x: 450, y: 150, type: 'pawn', player: 'black' },
        { id: 13, x: 550, y: 150, type: 'pawn', player: 'black' },
        { id: 14, x: 650, y: 150, type: 'pawn', player: 'black' },
        { id: 15, x: 750, y: 150, type: 'pawn', player: 'black' },
        { id: 16, x: 50, y: 650, type: 'pawn', player: 'white' },
        { id: 17, x: 150, y: 650, type: 'pawn', player: 'white' },
        { id: 18, x: 250, y: 650, type: 'pawn', player: 'white' },
        { id: 19, x: 350, y: 650, type: 'pawn', player: 'white' },
        { id: 20, x: 450, y: 650, type: 'pawn', player: 'white' },
        { id: 21, x: 550, y: 650, type: 'pawn', player: 'white' },
        { id: 22, x: 650, y: 650, type: 'pawn', player: 'white' },
        { id: 23, x: 750, y: 650, type: 'pawn', player: 'white' },
        { id: 24, x: 50, y: 750, type: 'rook', player: 'white' },
        { id: 26, x: 250, y: 750, type: 'bishop', player: 'white' },
        { id: 27, x: 350, y: 750, type: 'queen', player: 'white' },
        { id: 28, x: 450, y: 750, type: 'king', player: 'white' },
        { id: 29, x: 550, y: 750, type: 'bishop', player: 'white' },
        { id: 31, x: 750, y: 750, type: 'rook', player: 'white' },
        { id: 1, x: 150, y: 50, type: 'knight', player: 'black' },
        { id: 6, x: 650, y: 50, type: 'knight', player: 'black' },
        { id: 25, x: 150, y: 750, type: 'knight', player: 'white' },
        { id: 30, x: 650, y: 750, type: 'knight', player: 'white' },
      ]
    };

    //Create board
    this.board = new Board(this.global);

    //Setup pieces
    this.pieces = [];
    for(var i = 0;i < this.global.state.pieces.length;i++) {
      this.pieces.push(new Piece(this.global, this.global.state.pieces[i]));
    }

    //Setup network
    this.network = new Network(this.global);

    this.engineStartListener = this.global.emitter.on('engineStart', this.engineUpdate.bind(this));
    this.netReceivedListener = this.global.emitter.on('netReceived', this.netReceived.bind(this));
    Events.on(this.global.engine, 'collisionEnd', this.engineCollision.bind(this));
  }
  engineUpdate() {
    //Check if all are sleeping
    var isSleeping = true;
    for(var i = 0;i < this.pieces.length;i++) {
      if(!this.pieces[i].body.isSleeping) {
        isSleeping = false;
      }
    }

    //Continue if not sleeping
    if(!isSleeping) {
      this.global.state.isActive = true;
      var targetDelta = 1000/60;
      Engine.update(this.global.engine, targetDelta, 1);
      this.global.emitter.emit('engineUpdate');
      window.setTimeout(this.engineUpdate.bind(this), targetDelta);
    }
    else {
      this.global.state.isActive = false;
      this.global.state.player = this.global.state.player === 'white' ? 'black' : 'white';
      if(this.global.state.type !== 'local') {
        if(this.global.state.canPlay) {
          this.global.emitter.emit('netSend', {
            type: 'state',
            data: this.global.state
          });
        }
        this.global.state.canPlay = !this.global.state.canPlay;
      }
      this.global.emitter.emit('engineDone');
    }
  }
  engineCollision(event) {
    var pairs = event.pairs;
    for(var i = 0;i < pairs.length;i++) {
      var bodyA = pairs[i].bodyA;
      var bodyAPlayer = '';
      var bodyAPieceIndex = -1;
      var bodyACapturing = false;
      var bodyAIsTop = this.global.bounds.top.id === bodyA.id;
      var bodyAIsBottom = this.global.bounds.bottom.id === bodyA.id;
      var bodyB = pairs[i].bodyB;
      var bodyBPlayer = '';
      var bodyBPieceIndex = -1;
      var bodyBCapturing = false;
      var bodyBIsTop = this.global.bounds.top.id === bodyB.id;
      var bodyBIsBottom = this.global.bounds.bottom.id === bodyB.id;
      for(var j = 0;j < this.pieces.length;j++) {
        if(this.pieces[j].body.id === bodyA.id) {
          bodyAPlayer = this.pieces[j].state.player;
          bodyAPieceIndex = j;
          bodyACapturing = this.pieces[j].isCapturing;
        }
        if(this.pieces[j].body.id === bodyB.id) {
          bodyBPlayer = this.pieces[j].state.player;
          bodyBPieceIndex = j;
          bodyBCapturing = this.pieces[j].isCapturing;
        }
      }
      if(bodyA.id !== bodyB.id){
        if(bodyAPlayer === 'white' && bodyBIsTop && this.pieces[bodyAPieceIndex].state.type === 'pawn') {
          this.pieces[bodyAPieceIndex].state.type = 'queen';
          this.pieces[bodyAPieceIndex].update();
        }
        if(bodyBPlayer === 'white' && bodyAIsTop && this.pieces[bodyBPieceIndex].state.type === 'pawn') {
          this.pieces[bodyBPieceIndex].state.type = 'queen';
          this.pieces[bodyBPieceIndex].update();
        }
        if(bodyAPlayer === 'black' && bodyBIsBottom && this.pieces[bodyAPieceIndex].state.type === 'pawn') {
          this.pieces[bodyAPieceIndex].state.type = 'queen';
          this.pieces[bodyAPieceIndex].update();
        }
        if(bodyBPlayer === 'black' && bodyAIsBottom && this.pieces[bodyBPieceIndex].state.type === 'pawn') {
          this.pieces[bodyBPieceIndex].state.type = 'queen';
          this.pieces[bodyBPieceIndex].update();
        }
        if(bodyAPlayer.length > 0 && bodyBPlayer.length > 0) {
          if(bodyAPlayer !== bodyBPlayer) {
            if(bodyAPlayer !== this.global.state.player && bodyBCapturing) {
              if(this.pieces[bodyAPieceIndex].state.type === 'king') {
                alert(`${this.pieces[bodyAPieceIndex].state.player === 'white' ? 'Black' : 'White'} Wins!`);
              }
              this.pieces[bodyAPieceIndex].destroy();
              this.pieces.splice(bodyAPieceIndex, 1);
              this.global.state.pieces.splice(bodyAPieceIndex, 1);
            }
            else if(bodyBPlayer !== this.global.state.player && bodyACapturing) {
              if(this.pieces[bodyBPieceIndex].state.type === 'king') {
                alert(`${this.pieces[bodyBPieceIndex].state.player === 'white' ? 'Black' : 'White'} Wins!`);
              }
              this.pieces[bodyBPieceIndex].destroy();
              this.pieces.splice(bodyBPieceIndex, 1);
              this.global.state.pieces.splice(bodyBPieceIndex, 1);
            }
          }
        }
      }
    }
  }
  async netReceived(data) {
    if(data.type === 'state') {
      if(this.global.state.isActive) {
        await (new Promise((r) => {
          var e = this.global.emitter.on('engineDone', () => {
            e();
            r();
          });
        }));
      }
      this.global.state.type = data.data.type;
      this.global.state.player = data.data.player;
      this.global.state.canPlay = data.data.canPlay;
      var pieces = data.data.pieces;
      for(var j = 0;j < this.pieces.length;j++) {
        var found = false;
        for(var i = 0;i < pieces.length;i++) {
          if(pieces[i].id === this.pieces[j].state.id) {
            found = true;
          }
        }
        if(!found) {
          this.pieces[j].destroy();
          this.pieces.splice(j, 1);
          this.global.state.pieces.splice(j, 1);
          j--;
        }
      }
      this.global.emitter.emit('engineDone');
    }
  }
};

module.exports = Game;