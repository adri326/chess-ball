const copy = require('copy-to-clipboard');

class Network {
  constructor(global) {
    this.global = global;

    this.peer = null;
    this.type = null;
    this.connection = null;

    var urlSearch = new URLSearchParams(window.location.search);
    if(urlSearch.has('type')) {
      this.global.state.type = 'network';
      this.global.state.canPlay = false;
      this.peer = new Peer({ host: 'peer.chessin5d.net', port: 8000, path: '/', secure: true });
      this.type = urlSearch.get('type');
      this.peer.on('open', () => {
        if(this.type === 'host') {
          copy(`${window.location.origin}${window.location.pathname}?type=client&host=${this.peer.id}`);
          this.peer.on('connection', (c) => {
            this.connection = c;
            this.initConnection();
          });
        }
        if(this.type === 'client') {
          if(urlSearch.has('host')) {
            this.connection = this.peer.connect(urlSearch.get('host'));
            this.initConnection();
          }
        }
      });
    }

    this.netSendListener = this.global.emitter.on('netSend', (data) => {
      this.connection.send(data);
    });
  }
  initConnection() {
    this.connection.on('open', () => {
      this.global.state.canPlay = this.type === 'host';
      this.connection.on('data', async (data) => {
        if(this.global.state.isActive) {
          await (new Promise((r) => {
            var e = this.global.emitter.on('engineDone', () => {
              e();
              r();
            });
          }));
        }
        this.global.emitter.emit('netReceived', data);
      });
    });
  }
}

module.exports = Network;